create table user
(
    id      int auto_increment primary key,
    name    varchar(255)   not null,
    balance decimal(19, 2) not null
);

create table transaction
(
    id        int auto_increment primary key,
    timestamp timestamp      not null,
    user_from int            not null,
    user_to   int            not null,
    amount    decimal(19, 2) not null,
    constraint transaction_user_from_fk
        foreign key (user_from) references user (id),
    constraint transaction_user_to_fk
        foreign key (user_to) references user (id)
);
