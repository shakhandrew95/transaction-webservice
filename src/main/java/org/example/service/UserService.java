package org.example.service;

import org.apache.ibatis.session.SqlSession;
import org.example.dao.UserDao;
import org.example.exception.TransferException;
import org.example.model.BalanceAction;
import org.example.model.db.User;
import org.example.util.MyBatisUtil;

import javax.ejb.Stateless;
import java.util.List;

@Stateless
public class UserService {

    public List<User> get() {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory()
                .openSession()) {
            UserDao dao = sqlSession.getMapper(UserDao.class);
            return dao.get();
        }
    }

    public User get(Integer id) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory()
                .openSession()) {
            UserDao dao = sqlSession.getMapper(UserDao.class);
            return dao.getById(id);
        }
    }

    public int create(User user) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory()
                .openSession()) {
            UserDao dao = sqlSession.getMapper(UserDao.class);
            dao.create(user);
            sqlSession.commit();
            return user.getId();
        }
    }

    public void update(User user) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory()
                .openSession()) {
            UserDao dao = sqlSession.getMapper(UserDao.class);
            dao.update(user);
            sqlSession.commit();
        }
    }

    public void delete(String id) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory()
                .openSession()) {
            UserDao dao = sqlSession.getMapper(UserDao.class);
            dao.delete(id);
            sqlSession.commit();
        }
    }

    public void updateBalance(BalanceAction action) throws TransferException {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory()
                .openSession()) {
            UserDao dao = sqlSession.getMapper(UserDao.class);
            User user = dao.getById(action.getUserId());

            switch (action.getType()) {
                case WITHDRAW: {
                    if (user.getBalance().compareTo(action.getAmount()) < 0) {
                        throw new TransferException("The user " + user.getName() + " does not have enough money");
                    }
                    user.setBalance(user.getBalance().subtract(action.getAmount()));
                }
                case REPLENISH:
                    user.setBalance(user.getBalance().add(action.getAmount()));
            }

            dao.update(user);
            sqlSession.commit();
        }
    }

}
