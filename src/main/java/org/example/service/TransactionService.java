package org.example.service;

import org.apache.ibatis.session.SqlSession;
import org.example.dao.TransactionDao;
import org.example.dao.UserDao;
import org.example.exception.TransferException;
import org.example.model.SearchTransaction;
import org.example.model.Transfer;
import org.example.model.db.Transaction;
import org.example.model.db.User;
import org.example.util.MyBatisUtil;

import javax.ejb.Stateless;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

@Stateless
public class TransactionService {

    public List<Transaction> search(SearchTransaction search) {
        try (SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory()
                .openSession()) {
            TransactionDao dao = sqlSession.getMapper(TransactionDao.class);
            return dao.search(search);
        }
    }

    public void transfer(Transfer transfer) throws TransferException {
        SqlSession sqlSession = MyBatisUtil.getSqlSessionFactory()
                .openSession();
        try {
            UserDao userDao = sqlSession.getMapper(UserDao.class);
            TransactionDao transactionDao = sqlSession.getMapper(TransactionDao.class);

            User userFrom = Optional.ofNullable(userDao.getById(transfer.getUserFrom()))
                    .orElseThrow(() -> new TransferException("User from not found."));
            User userTo = Optional.ofNullable(userDao.getById(transfer.getUserTo()))
                    .orElseThrow(() -> new TransferException("User to not found."));

            if (userFrom.getBalance().compareTo(transfer.getAmount()) < 0) {
                throw new TransferException("The user's " + userFrom.getName() + " balance is less than the amount written off");
            }

            userFrom.setBalance(userFrom.getBalance().subtract(transfer.getAmount()));
            userTo.setBalance(userTo.getBalance().add(transfer.getAmount()));

            Transaction transaction = new Transaction(LocalDateTime.now(), transfer.getUserFrom(),
                    transfer.getUserTo(), transfer.getAmount());

            userDao.update(userFrom);
            userDao.update(userTo);
            transactionDao.save(transaction);

            sqlSession.commit();
        } catch (TransferException e) {
            sqlSession.rollback();
            throw e;
        }  catch (Exception e) {
            sqlSession.rollback();
            throw new TransferException("Something went wrong.");
        } finally {
            sqlSession.close();
        }
    }

}
