package org.example.model;

import java.math.BigDecimal;

public class BalanceAction {

    private Integer userId;

    private BalanceActionType type;

    private BigDecimal amount;

    public int getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public BalanceActionType getType() {
        return type;
    }

    public void setType(BalanceActionType type) {
        this.type = type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
