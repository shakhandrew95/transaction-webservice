package org.example.model;

import java.time.LocalDate;

public class SearchTransaction {

    private final Integer userFrom;

    private final Integer userTo;

    private final LocalDate startDate;

    private final LocalDate endDate;

    public SearchTransaction(Integer userFrom, Integer userTo, LocalDate startDate, LocalDate endDate) {
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.startDate = startDate;
        this.endDate = endDate;
    }

    public Integer getUserFrom() {
        return userFrom;
    }

    public Integer getUserTo() {
        return userTo;
    }

    public LocalDate getStartDate() {
        return startDate;
    }

    public LocalDate getEndDate() {
        return endDate;
    }
}
