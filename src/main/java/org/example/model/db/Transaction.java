package org.example.model.db;

import java.math.BigDecimal;
import java.time.LocalDateTime;

public class Transaction {

    private Integer id;

    private LocalDateTime timestamp;

    private Integer userFrom;

    private Integer userTo;

    private BigDecimal amount;

    public Transaction() {
    }

    public Transaction(LocalDateTime timestamp, Integer userFrom, Integer userTo, BigDecimal amount) {
        this.timestamp = timestamp;
        this.userFrom = userFrom;
        this.userTo = userTo;
        this.amount = amount;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
    }

    public Integer getUserFrom() {
        return userFrom;
    }

    public void setUserFrom(Integer userFrom) {
        this.userFrom = userFrom;
    }

    public Integer getUserTo() {
        return userTo;
    }

    public void setUserTo(Integer userTo) {
        this.userTo = userTo;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public void setAmount(BigDecimal amount) {
        this.amount = amount;
    }
}
