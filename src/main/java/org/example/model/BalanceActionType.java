package org.example.model;

public enum BalanceActionType {

    WITHDRAW,
    REPLENISH

}
