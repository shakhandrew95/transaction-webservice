package org.example.rest;

import org.example.exception.TransferException;
import org.example.model.SearchTransaction;
import org.example.model.Transfer;
import org.example.model.db.Transaction;
import org.example.service.TransactionService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.time.LocalDate;
import java.util.List;

@Path("/transaction")
@Produces("application/json")
public class TransactionController {

    @Inject
    private TransactionService service;

    @GET
    @Path("/history")
    public Response history(@QueryParam("userFrom") Integer userFrom,
                            @QueryParam("userTo") Integer userTo,
                            @QueryParam("startDate") LocalDate startDate,
                            @QueryParam("endDate") LocalDate endDate) {

        List<Transaction> transactions = service.search(new SearchTransaction(userFrom, userTo, startDate, endDate));
        return Response.ok(transactions).build();
    }

    @POST
    @Path("/transfer")
    public Response transfer(Transfer transfer) {
        try {
            service.transfer(transfer);
            return Response.ok().build();
        } catch (TransferException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }


}
