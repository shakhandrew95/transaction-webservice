package org.example.rest;

import org.example.exception.TransferException;
import org.example.model.BalanceAction;
import org.example.model.db.User;
import org.example.service.UserService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.List;

@Path("/users")
@Produces("application/json")
public class UserController {

    @Inject
    private UserService userService;

    @GET
    @Path("/{id}")
    public Response get(@PathParam("id") Integer id) {
        User user = userService.get(id);
        return Response.ok(user).build();
    }

    @GET
    public Response get() {
        List<User> users = userService.get();
        return Response.ok(users).build();
    }

    @POST
    public Response create(User user) {
        int id = userService.create(user);
        return Response.ok(id).build();
    }

    @PUT
    public Response update(User user) {
        userService.update(user);
        return Response.ok().build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") String id) {
        userService.delete(id);
        return Response.ok().build();
    }

    @POST
    @Path("/balance")
    public Response updateBalance(BalanceAction action) {
        try {
            userService.updateBalance(action);
            return Response.ok().build();
        } catch (TransferException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(e.getMessage()).build();
        }
    }

}
