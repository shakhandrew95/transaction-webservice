package org.example.util;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

import java.io.IOException;
import java.io.Reader;

public class MyBatisUtil {

    private static final String CONFIG = "mybatis-config.xml";

    private static SqlSessionFactory factory;

    private MyBatisUtil() {
    }

    public static SqlSessionFactory getSqlSessionFactory() {
        if (factory == null) {
            Reader reader;
            try {
                reader = Resources.getResourceAsReader(CONFIG);
            } catch (IOException e) {
                throw new RuntimeException(e.getMessage());
            }
            factory = new SqlSessionFactoryBuilder().build(reader);
        }
        return factory;
    }

}
