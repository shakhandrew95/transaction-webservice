package org.example.dao;

import org.example.model.db.User;

import java.util.List;

public interface UserDao {

    User getById(Integer id);

    List<User> get();

    void create(User user);

    void update(User user);

    void delete(String id);

}
