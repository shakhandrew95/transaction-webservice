package org.example.dao;

import org.apache.ibatis.annotations.Param;
import org.example.model.SearchTransaction;
import org.example.model.db.Transaction;

import java.util.List;

public interface TransactionDao {

    List<Transaction> search(@Param("search") SearchTransaction search);

    void save(Transaction transaction);
}
